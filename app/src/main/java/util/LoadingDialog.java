package util;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by mark on 14/12/17.
 */

public class LoadingDialog {
    private static ProgressDialog progressDialog;

    public static void show(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hide() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
