package com.markmorcos.a5omasy.activities;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.markmorcos.a5omasy.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import util.Constants;
import util.LoadingDialog;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Constants.defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void login(View view) {
        JSONObject request = new JSONObject();
        try {
            request.put("access_code", Constants.accessCode);
            request.put("email", email.getText().toString());
            request.put("password", password.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LoadingDialog.show(this);
        Volley
                .newRequestQueue(this)
                .add(new JsonObjectRequest(
                        Request.Method.POST,
                        Constants.baseURL + "customer/login_password",
                        request,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                LoadingDialog.hide();
                                try {
                                    if (response.has("status") && response.getBoolean("status")) {
                                        Constants.defaultSharedPreferences.edit().putString("session_code", response.getJSONObject("response").getString("session_code")).apply();
                                        startActivity(new Intent(LoginActivity.this, ListActivity.class));
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                System.out.println(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                LoadingDialog.hide();
                                System.out.println(error);
                            }
                        }));
    }
}
