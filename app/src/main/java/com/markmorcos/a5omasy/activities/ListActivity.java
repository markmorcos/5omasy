package com.markmorcos.a5omasy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.markmorcos.a5omasy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import util.Constants;
import util.LoadingDialog;

public class ListActivity extends AppCompatActivity {

    ArrayList<String> listArray;

    @BindView(R.id.list)
    EditText date;
    @BindView(R.id.list)
    EditText hour;
    @BindView(R.id.list)
    Spinner list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listArray = new ArrayList<>();
        list();
    }

    public void list() {
        LoadingDialog.show(this);
        Volley
                .newRequestQueue(this)
                .add(new JsonObjectRequest(
                        Request.Method.POST,
                        Constants.baseURL + "locations/list",
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                LoadingDialog.hide();
                                if (response.has("session_code")) {
                                    try {
                                        if (response.has("status") && response.getBoolean("status")) {
                                            JSONArray listArrayObject = response.getJSONObject("response").getJSONArray("locations");
                                            listArray.clear();
                                            for (int i = 0; i < listArrayObject.length(); ++i) {
                                                listArray.add(listArrayObject.getJSONObject(i).getString("location_name"));
                                            }
                                            list.setAdapter(new ArrayAdapter<>(ListActivity.this, R.layout.support_simple_spinner_dropdown_item, listArray));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                System.out.println(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                LoadingDialog.hide();
                                System.out.println(error);
                            }
                        }));
    }

    public void search(View view) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra("date", date.getText());
        intent.putExtra("hour", Integer.parseInt(hour.getText().toString()));
        intent.putExtra("location_id", Integer.parseInt(hour.getText().toString()));
        startActivity(intent);
    }
}
