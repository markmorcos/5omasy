package com.markmorcos.a5omasy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.markmorcos.a5omasy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import util.Constants;
import util.LoadingDialog;

public class SearchActivity extends AppCompatActivity {

    ArrayList<String> searchArray;

    @BindView(R.id.search)
    ListView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }

    public void list() {
        LoadingDialog.show(this);
        Volley
                .newRequestQueue(this)
                .add(new JsonObjectRequest(
                        Request.Method.POST,
                        Constants.baseURL + "venues/search",
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                LoadingDialog.hide();
                                if (response.has("session_code")) {
                                    try {
                                        if (response.has("status") && response.getBoolean("status")) {
                                            JSONArray searchArrayObject = response.getJSONObject("response").getJSONArray("locations");
                                            searchArray.clear();
                                            for (int i = 0; i < searchArrayObject.length(); ++i) {
                                                searchArray.add(searchArrayObject.getJSONObject(i).getString("location_name"));
                                            }
                                            search.setAdapter(new ArrayAdapter<>(SearchActivity.this, R.layout.support_simple_spinner_dropdown_item, searchArray));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                System.out.println(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                LoadingDialog.hide();
                                System.out.println(error);
                            }
                        }));
    }
}
